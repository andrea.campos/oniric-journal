import { useState } from "react";
import { Redirect, useHistory, useParams } from "react-router-dom";
import DreamForm from "../../components/DreamForm";

import "./styles.scss";

import handUp from "../../assets/handUp.png";
import handDown from "../../assets/handDown.png";

function EditDream({ today, dreamList, setDreamList }) {
  // Sacamos el sueño que queremos editar usando la id que tenemos en la url
  const history = useHistory();
  const { id } = useParams();
  const initialDream = dreamList.find((dream) => dream.id === id);

  const [editForm, setEditForm] = useState({
    id: initialDream?.id,
    dreamDescription: initialDream?.dreamDescription,
    date: initialDream?.date,
  });

  const handleChangeInput = (event) => {
    const newValue = event.target.value;
    const inputName = event.target.name;

    setEditForm({
      ...editForm,
      [inputName]: newValue,
    });
  };

  function handleSubmit(ev) {
    ev.preventDefault();

    const newDreamList = dreamList.map((dream) => {
      if (dream.id === editForm.id) {
        return editForm;
      } else {
        return dream;
      }
    });

    setDreamList(newDreamList);
    history.push("/my-dream");
  }

  if (!editForm.id) {
    return <Redirect to="/my-dream" />;
  }

  return (
    <div className="EditDream">
      <img className="EditDream__hand-up" src={handUp} alt="Hand Up" />
      <h1 className="EditDream__title">{today}</h1>
      <DreamForm
        handleSubmit={handleSubmit}
        handleChangeInput={handleChangeInput}
        form={editForm}
      />
      <img className="EditDream__hand-down" src={handDown} alt="Hand Down" />
    </div>
  );
}

export default EditDream;
