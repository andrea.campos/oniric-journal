import Dream from '../../components/Dream';

import "./styles.scss";

function MyDream({ dreamList, today, deleteDream }) {
    const filteredDream = dreamList.filter((item) => {
        return (item.date === today);
    });

    return (
        <main className="MyDream">
            <Dream filteredDream={filteredDream} deleteDream={deleteDream} />
        </main>
    )
}

export default MyDream;