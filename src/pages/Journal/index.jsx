import { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { format } from "date-fns";
import { Link } from "react-router-dom";

import "./styles.scss";

import quartz from "../../assets/quartz.png";

function Journal({ dreamList, deleteDream }) {
  const [startDate, setStartDate] = useState(new Date());

  const formatCalendarDate = format(startDate, "MM/dd/yyyy");

  const filteredCalendarDream = dreamList.filter((item) => {
    return item.date === formatCalendarDate;
  });

  return (
    <div className="Journal">
      <img className="Journal__quartz-left" src={quartz} alt="Quartz Left" />
      <h1 className="Journal__title">My journal</h1>
      <img className="Journal__quartz-right" src={quartz} alt="Quartz Right" />
      <DatePicker
        className="Journal__calendar"
        inline
        selected={startDate}
        onChange={(date) => setStartDate(date)}
      />
      {filteredCalendarDream.length > 0 ? null : (
        <p className="Journal__no-dreams">No dreams for this day</p>
      )}{" "}
      {" "}
      {filteredCalendarDream.length === 1 ? (
        <h2 className="Journal__message">
          You have {filteredCalendarDream.length} dream
        </h2>
      ) : filteredCalendarDream.length > 1 ? (
        <h2 className="Journal__message">
          You have {filteredCalendarDream.length} dreams
        </h2>
      ) : null}
      {/* Pintar aquí  la lista filtrada */}
      {filteredCalendarDream.map((myCalendarDream) => {
        return (
          <div className="Journal__calendar-dream" key={myCalendarDream.id}>
            <p className="Journal__dream-description">
              {myCalendarDream.dreamDescription}
            </p>
            <div className="Journal__buttons">
              <button className="Journal__buttons__btn">
                <Link
                  className="Journal__buttons__link"
                  to={`/edit-dream/${myCalendarDream.id}`}
                >
                  Edit my dream
                </Link>
              </button>
              <button
                className="Journal__buttons__btn"
                onClick={() => deleteDream(myCalendarDream.id)}
              >
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default Journal;
