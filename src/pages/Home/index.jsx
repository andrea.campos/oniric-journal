import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import "./styles.scss";

function Home() {

  return (
    <div className="Home">
      <h1 className="Home__title fade-in">What have you dreamed today?</h1>
      <Link to="/new-dream" className="Home__link fade-in">
        <FontAwesomeIcon className="Home__link__icon" icon={faPlus} />
        <span className="Home__link__text">new entry</span>
      </Link>
    </div>
  )
}

export default Home;