import DreamForm from "../../components/DreamForm";
import { format } from "date-fns";

import "./styles.scss";

import handUp from "../../assets/handUp.png";
import handDown from "../../assets/handDown.png";

function NewDream({ handleChangeInput, handleSubmit, form }) {
  const today = format(new Date(), "'Today, ' eeee");
  return (
    <div className="NewDream">
      <img className="NewDream__hand-up" src={handUp} alt="Hand Up" />
      <h1 className="NewDream__title">{today}</h1>
      <DreamForm
        handleSubmit={handleSubmit}
        handleChangeInput={handleChangeInput}
        form={form}
      />
      <img className="NewDream__hand-down" src={handDown} alt="Hand Down" />
    </div>
  );
}

export default NewDream;
