import { Link } from 'react-router-dom';
import { format } from 'date-fns';

import './styles.scss';

function Dream({ filteredDream, deleteDream }) {

  const today = format(new Date(), "'Today, ' eeee");

  return (
    <div className="Dream">
      <h2 className="Dream__title">{today}</h2>
      {filteredDream.length > 0 ? null : <p className="Dream__message">No dreams for today yet</p>}
      {filteredDream.map((myDream) => {
        return (
          <div className="Dream__container" key={myDream.id}>
            <p>{myDream.dreamDescription}</p>
            <div className="Dream__buttons">
              <button className="Dream__buttons__btn">
                <Link to={`/edit-dream/${myDream.id}`} className="Dream__buttons__link">
                  Edit my dream
              </Link>
              </button>
              <button
                className="Dream__buttons__btn"
                onClick={() => deleteDream(myDream.id)}
              >
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}
export default Dream;
