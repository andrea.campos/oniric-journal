import { useState } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import "./styles.scss";
import logo from "../../assets/logo.png";

function Header() {
  const [showMenu, setShowMenu] = useState(false);

  const handleShowMenu = () => {
    setShowMenu(!showMenu);
  };

  return (
    <div className="Header">
      {/* Mobile header */}
      <nav className="Header__nav__mobile">
        <div className="Header__nav__mobile__container">
          <Link to="/" className="Header__nav__mobile__container__logo">
            <img className="logo__mobile" src={logo} alt="Logo" />
          </Link>
          <FontAwesomeIcon
            className="Header__nav__mobile__container__burger"
            onClick={handleShowMenu}
            icon={faBars}
          />
        </div>
        {showMenu ? (
          <ul className="Header__nav__mobile__list">
            <li className="Header__nav__mobile__list__item">
              <button className="Header__nav__mobile__list__button" onClick={showMenu}>
                <Link to="/journal" className="Header__link__mobile">
                  My Journal
              </Link>
              </button>
            </li>
            <li className="Header__nav__mobile__list__item">
              <button className="Header__nav__mobile__list__button" onClick={showMenu}>
                <Link to="/new-dream" className="Header__link__mobile">
                  New Dream
              </Link>
              </button>
            </li>
          </ul>
        ) : null}
      </nav>

      {/* Desktop Header */}
      <nav className="Header__nav__desktop">
        <ul className="Header__nav__desktop__list">
          <li className="Header__nav__list__item">
            <Link to="/journal" className="Header__nav__list__link">
              My Journal
            </Link>
          </li>
          <li className="Header__nav__list__item">
            <Link to="/" className="Header__nav__list__link">
              <img
                className="Header__nav__list__img"
                src={logo}
                alt="Logo"
              />
            </Link>
          </li>
          <li className="Header__nav__list__item">
            <Link to="/new-dream" className="Header__nav__list__link">
              New Dream
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Header;
