import { useState } from "react";
import { Link } from "react-router-dom";

import "./styles.scss";

function DreamForm({ handleSubmit, handleChangeInput, form }) {
  return (
    <form className="DreamForm" onSubmit={handleSubmit}>
      <fieldset className="DreamForm__fieldset">
        <label htmlFor="dreamDescription"></label>
        <textarea
          className="DreamForm__fieldset__textarea"
          onChange={handleChangeInput}
          id="dreamDescription"
          type="dreamDescription"
          value={form.dreamDescription}
          name="dreamDescription"
          placeholder="Write your dream"
          required
        ></textarea>
      </fieldset>
      <div className="DreamForm__btn-container">
        <button className="DreamForm__btn" type="submit">
          Save
        </button>
        <button className="DreamForm__btn DreamForm__btn--cancel">
          <Link className="DreamForm__btn__link" to="/">Cancel</Link>
        </button>
      </div>
    </form>
  );
}

export default DreamForm;
