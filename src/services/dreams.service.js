import db from "../config/lowdb";

const DREAMS_DB = "dreams";

export const getDreams = async () => {
  return db.get(DREAMS_DB).value();
};

export const postDream = async (newEntry) => {
  db.get(DREAMS_DB).push(newEntry).write();
};

export const putNewDreamList = async (id) => {
  db.get(DREAMS_DB).remove({ id }).write();
};
