import { useState, useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';

import {
  getDreams,
  postDream,
  putNewDreamList,
} from './services/dreams.service';

import Home from './pages/Home';
import NewDream from './pages/NewDream';
import MyDream from './pages/MyDream';
import Journal from './pages/Journal';
import EditDream from './pages/EditDream';

import useForm from './hooks/useForm';

import Header from './components/Header';

import './App.scss';

function App() {
  const [dreamList, setDreamList] = useState([]);
  const [form, today, setForm, handleChangeInput, newEntry] = useForm();

  const history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();

    addDream(newEntry);

    setForm({
      id: '',
      dreamDescription: '',
    });

    history.push('/my-dream');
  };

  useEffect(() => {
    getDreams()
      .then((response) => {
        setDreamList(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  async function addDream(newEntry) {
    setDreamList([...dreamList, newEntry]);

    await postDream(newEntry);
  }

  async function deleteDream(id) {
    const newDreamList = dreamList.filter((dream) => dream.id !== id);

    setDreamList(newDreamList);
    await putNewDreamList(id);
  }

  return (
    <div className="App">
      <Header />

      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>

        <Route path="/new-dream" exact>
          <NewDream
            handleChangeInput={handleChangeInput}
            handleSubmit={handleSubmit}
            form={form}
          />
        </Route>

        <Route path="/edit-dream/:id" exact>
          <EditDream
            today={today}
            dreamList={dreamList}
            setDreamList={setDreamList}
          />
        </Route>

        <Route path="/my-dream" exact>
          <MyDream
            dreamList={dreamList}
            deleteDream={deleteDream}
            today={today}
          />
        </Route>

        <Route path="/journal" exact>
          <Journal dreamList={dreamList} deleteDream={deleteDream} />
        </Route>
      </Switch>
    </div>
  );
}

export default App;