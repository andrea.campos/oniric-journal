import { useState } from "react";
import { v4 as uuid } from "uuid";
import { format } from 'date-fns';

function useForm() {
    const [form, setForm] = useState({
        dreamDescription: "",
    });

    const today = format(new Date(), "MM/dd/yyyy");
    const handleChangeInput = (event) => {
        const newValue = event.target.value;
        const inputName = event.target.name;
        setForm({
            ...form,
            [inputName]: newValue,
        });
    };
    const newEntry = {
        ...form,
        id: uuid(),
        date: today,
    };
    return [form, today, setForm, handleChangeInput, newEntry];
}
export default useForm;
