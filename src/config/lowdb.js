import low from 'lowdb';
import LocalStorage from 'lowdb/adapters/LocalStorage';

const adapter = new LocalStorage('db');
const db = low(adapter);

db.defaults({
    dreams: [
        {
            id: 'c186440a-537b-401c-8007-d39ce55fca06',
            date: '02/18/2021',
            dreamDescription: "I dreamt that my mom was a serial killer and my family had to escape from our house. We packed our things and drove away. After a few months, we came back to gather a few things we left. I ran inside, and the whole house was filled with tipped-over furniture, cobwebs, flickering lights, etc. The stereotypical creepy house. I crept into my room to grab my bed sheets then ran when I realized my mom was in the shower. I was home free! Grabbing my cat, I sprinted outside, threw everything in the back of the car, then got into the driver’s seat to zoom off. Suddenly, a figure approached the back door. I stepped out of the car to see my mom holding a bag. She had a poker face and held out a bag of cat food. “You might wanna take this with you,” she murmured. I quickly thanked her, grabbed the bag, hopped into the car, and sped away.",
        },
    ],
}).write();

export default db;
